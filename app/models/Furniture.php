<?php

use MyApp\models\Product;

class Furniture extends Product
{
    private $dimension;

    public function __construct()
    {
        parent::__construct();
        $this->setType(2);
    }

    public function getAttribute()
    {
        return $this->dimension;
    }

    public function setAttribute($attribute)
    {
        $this->dimension = "{$attribute['height']}x{$attribute['width']}x{$attribute['length']}";
    }


}
