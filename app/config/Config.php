<?php

namespace MyApp\config;

// this file basically makes it easy to set a different environment for the project
class Config
{
    const ASSETS = 'app/assets';
    const APP = 'app';
    const PUSH = 'add-product';
    const POP = './';
    const BASEURL = 'https://bassel-abdelkader-junior-test.herokuapp.com/';
    const DB_HOST = 'eu-cdbr-west-02.cleardb.net';
    const DB_NAME = 'heroku_8dd18409e99ee82';
    const DB_USERNAME = 'bdc546b40fd96e';
    const DB_PASSWORD = 'b0a4ad57';
    const CHARSET = 'utf8mb4';

}
//mysql://bdc546b40fd96e:b0a4ad57@eu-cdbr-west-02.cleardb.net/heroku_8dd18409e99ee82?reconnect=true