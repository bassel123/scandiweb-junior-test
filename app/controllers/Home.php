<?php

use MyApp\config\Config as Config;
use MyApp\core\Controller;



class Home extends Controller
{
    public function index()
    {

        if ($_POST['switcher'] ?? false) {
            $item = $this->model($_POST['switcher']);
            $item->setSKU($_POST['sku']);
            $item->setName($_POST['name']);
            $item->setPrice($_POST['price']);
            $item->setAttribute($_POST['attribute']);

            // reloading the page on successful inserts
            if ($item->insertProduct() > 0 && $item->insertProductValue() > 0) {
                header("Location: " . Config::BASEURL);
            }
        }
        //any item type used as they all share the same implementation
        elseif ($_POST['sku'] ?? false) {
            $sku = $_POST['sku'];

            if ($this->model("Book")->deleteProducts($sku)) {
                header("Location: " . Config::BASEURL);
            }
        }


        $this->view('home/index', $this->model("Book")->getAllProducts());
        $this->view('layouts/footer');
    }
}
