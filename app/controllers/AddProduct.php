<?php

use MyApp\core\Controller;

class AddProduct extends Controller
{
    // WHEN A GET REQUEST IS SENT TO THE HOME PAGE

    public function index()
    {
        // Displays the view
        $this->view('product/addProduct');
        $this->view('layouts/footer');
    }
}
