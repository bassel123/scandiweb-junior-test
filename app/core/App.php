<?php

namespace MyApp\core;

class App
{
    // adhering to the MVC architecture and setting the default values

    private $controller = 'Home';
    private $method = 'index';

    public function __construct()
    {
        $url = $this->parseUrl();

        //checking that there exista controller for the path required
        if (file_exists("app/controllers/$url[0].php")) {
            $this->controller = $url[0];
            unset($url[0]);
        }

        //initializing the controller to the home page
        require("app/controllers/$this->controller.php");
        $this->controller = new $this->controller;

        if ($url[1] ?? false) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        // calls themethod correspondng to current controller
        call_user_func([$this->controller, $this->method]);
    }

    public function parseUrl()
    {
        if ($_GET['url'] ?? false) {
            // Filters for special characters.
            $url = filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL);
            $url = explode('/', str_replace('-', '', ucwords($url, '-')));
            return $url;
        }
    }
}
