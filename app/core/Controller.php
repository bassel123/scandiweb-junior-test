<?php

namespace MyApp\core;

// Controllers methods are defined here.
class Controller
{

    // using the view method to iterate through query retrieved.
    public function view($view, $data = "")
    {
        require("app/views/$view.php");
    }

//creating the model instance

    public function model($model)
    {
        require("app/models/$model.php");
        return new $model;
    }
}
